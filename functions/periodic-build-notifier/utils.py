from pathlib import Path
from typing import List, Tuple
from zipfile import ZipFile


def get_secret(name: str) -> str:
    """
    Return a secret mapped as a file by OpenFAAS.

    Args:
        name: name of the secret

    Returns:
        Value secret value from the file.
    """

    return open(get_secret_path(name), "r").read().strip()


def get_secret_path(name: str) -> Path:
    """
    Return the given secrets' absolute path.

    Args:
        name: name of the secret

    Returns:
        Absolute path for the named secret.
    """

    return Path(f"/var/openfaas/secrets/{name}").absolute()


def compress_content(contents: List[Tuple[str, str]]) -> Path:
    """
    Compress multiple string content into a zip file and return the resulting zip file's
    path.

    Args:
        contents: list of filename-content pairs to compress

    Returns:
        Path for the resulting zip file.
    """

    zip_path = Path("build_logs.zip")

    with ZipFile(zip_path, "w") as f:
        for file_name, content in contents:
            f.writestr(f"{file_name}.txt", content)

    return zip_path
