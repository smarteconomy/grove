import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

from .utils import get_secret


def send_email(subject: str, body: str, attachment: Path):
    """
    Prepare the email's content, attach the logs file and send the email.

    Args:
        subject: subject of the email
        body: text content of the email
        attachment: path to the attachment

    Returns:
        No return values
    """

    sender = os.getenv("EMAIL_SENDER")
    recipient = os.getenv("EMAIL_RECIPIENT")

    job_logs = MIMEBase("application", "zip")
    job_logs.set_payload(attachment.read_bytes())
    encoders.encode_base64(job_logs)

    job_logs.add_header(
        "Content-Disposition", f"attachment; filename= {attachment.name}"
    )

    message = MIMEMultipart()
    message["From"] = sender
    message["To"] = recipient
    message["Subject"] = subject
    message.attach(MIMEText(body, "plain", "utf-8"))
    message.attach(MIMEText(body, "text/html", "utf-8"))
    message.attach(job_logs)

    email_session = smtplib.SMTP(
        os.getenv("EMAIL_SMTP_HOST"), int(os.getenv("EMAIL_SMTP_PORT"))
    )

    email_session.starttls()

    email_session.login(
        os.getenv("EMAIL_SMTP_USER"), get_secret("email-sender-password")
    )

    email_session.sendmail(sender, recipient, message.as_string())
    email_session.quit()
