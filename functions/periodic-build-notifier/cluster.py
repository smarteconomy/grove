from typing import Callable, List, Optional, Tuple

from kubernetes.client import ApiClient, Configuration, CoreV1Api
from kubernetes.client.models import V1Pod, V1PodList

from .utils import get_secret, get_secret_path


def configure_client() -> CoreV1Api:
    """
    Configure the Kubernetes client and return a core v1 API client for later use.

    Although returning the client wouldn't be necessary, since only v1 resources are
    used in Grove it is safe to return the v1 API client.

    Returns:
        Core v1 API client for later use.
    """

    # OpenFAAS expands the service account secret, therefore the data.token key is
    # available as token. The value is coming from periodic-build-notifier-access-token.
    conf = Configuration(
        host="https://kubernetes.default.svc",
        api_key={"authorization": get_secret("token")},
        api_key_prefix={"authorization": "Bearer"},
    )

    conf.ssl_ca_cert = get_secret_path("ca.crt")

    return CoreV1Api(api_client=ApiClient(conf))


def get_failed_pods(
    client: CoreV1Api,
    namespace: str,
    predicate: Optional[Callable[[V1Pod], V1Pod]] = None,
) -> V1PodList:
    """
    Get the failed pods from the given namespace, and optionally filter the result with
    a predicate function.

    Predicate function is available as the field selectors are not supporting regex or
    other kind of filtering.

    Args:
        client: CoreV1Api Kubernetes API client
        namespace: namespace where pods are looked up
        predicate: function to filter pods by their attributes

    Returns:
        List of pods filtered by the optional predicate function.
    """

    pods: V1PodList = client.list_namespaced_pod(
        namespace=namespace, field_selector="status.phase=Failed"
    )

    if predicate is not None:
        pods.items = filter(predicate, pods.items)

    return pods


def get_pod_logs(client: CoreV1Api, pods: V1PodList) -> List[Tuple[str, str]]:
    """
    Get the logs for the given list of pods and return the list of logs per pod name.

    Args:
        client: CoreV1Api Kubernetes API client
        pods: list of pods to retrieve logs from

    Returns:
        List of tuple of pod-log pairs.
    """

    return [
        (
            pod.metadata.name,
            client.read_namespaced_pod_log(
                name=pod.metadata.name, namespace=pod.metadata.namespace
            ),
        )
        for pod in pods.items
    ]
