variable "cluster_name" {
  type        = string
  default     = null
  description = "Grove cluster name the Droplet can access."
}

variable "image" {
  type        = string
  description = "Droplet image."
}

variable "name" {
  type        = string
  description = "Droplet name."
}

variable "region" {
  type        = string
  description = "Droplet region."
}

variable "size" {
  type        = string
  default     = "s-1vcpu-1gb"
  description = "Droplet size."
}

variable "backups" {
  type        = bool
  default     = true
  description = "Enable backups for the Droplet."
}

variable "ssh_keys" {
  type        = list(string)
  default     = []
  description = "SSH keys used for the Droplet."
}

variable "tags" {
  type        = list(string)
  default     = []
  description = "Tags assigned to the Droplet."
}

variable "volume_ids" {
  type        = list(string)
  default     = []
  description = "List of volume IDs attached to the Droplet."
}

variable "vpc_uuid" {
  type        = string
  description = "ID of the VPC which is assigned to the Droplet."
}

variable "user_data" {
  type        = string
  default     = null
  description = "User data run on the Droplet at the first start."
}
