output "droplet_urn" {
  value = digitalocean_droplet.droplet.urn
}

output "droplet_id" {
  value = digitalocean_droplet.droplet.id
}

output "droplet_ipv4" {
  value     = digitalocean_droplet.droplet.ipv4_address
  sensitive = true
}

output "droplet_key_pair" {
  value     = tls_private_key.private_key
  sensitive = true
}
