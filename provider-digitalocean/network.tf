####################################################################################################
## VPC: A virtual private cloud which will hold everything else
####################################################################################################

resource "digitalocean_vpc" "main_vpc" {
  name     = "${var.cluster_name}-vpc"
  region   = var.do_region
  ip_range = var.vpc_ip_range
}

# https://github.com/digitalocean/terraform-provider-digitalocean/issues/446
resource "time_sleep" "wait_for_vpc_members_to_be_deleted" {
  depends_on = [digitalocean_vpc.main_vpc]
  destroy_duration = "120s"
}
