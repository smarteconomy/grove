####################################################################################################
## AWS MongoDB Atlas
####################################################################################################

variable "mongodbatlas_project_id" { type = string }

variable "mongodb_provider_name" { default = "AWS" }

variable "mongodb_version" { default = "4.2" }
variable "mongodb_instance_size" { default = "M10" }
variable "mongodb_cluster_type" { default = "REPLICASET" }
variable "mongodb_num_shards" { default = 1 }
variable "mongodb_electable_nodes" { default = 3 }

variable "mongodb_backup_enabled" { default = true }
variable "mongodb_backup_retention_period" { default = 35 }

variable "mongodb_encryption_at_rest" { default = false }
variable "mongodb_auto_scaling_disk_gb_enabled" { default = true }
variable "mongodb_auto_scaling_compute_enabled" { default = false }

variable "mongodb_read_only_nodes" { default = null }
variable "mongodb_analytics_nodes" { default = null }
variable "mongodb_disk_size_gb" { default = null }
variable "mongodb_disk_iops" { default = null }
variable "mongodb_volume_type" { default = null }
variable "mongodb_auto_scaling_min_instances" { default = null }
variable "mongodb_auto_scaling_max_instances" { default = null }

variable "mongodb_atlas_cidr_block" { default = "192.168.248.0/21" }

module "mongodb" {
  source = "./mongodb"

  mongodbatlas_project_id = var.mongodbatlas_project_id

  cluster_name = var.cluster_name
  region_name  = var.aws_region

  tutor_instances = var.tutor_instances

  provider_name                = var.mongodb_provider_name
  mongodb_version              = var.mongodb_version
  instance_size                = var.mongodb_instance_size
  cluster_type                 = var.mongodb_cluster_type
  num_shards                   = var.mongodb_num_shards
  electable_nodes              = var.mongodb_electable_nodes
  read_only_nodes              = var.mongodb_read_only_nodes
  analytics_nodes              = var.mongodb_analytics_nodes
  disk_size_gb                 = var.mongodb_disk_size_gb
  disk_iops                    = var.mongodb_disk_iops
  volume_type                  = var.mongodb_volume_type
  backup_enabled               = var.mongodb_backup_enabled
  encryption_at_rest           = var.mongodb_encryption_at_rest
  auto_scaling_disk_gb_enabled = var.mongodb_auto_scaling_disk_gb_enabled
  auto_scaling_compute_enabled = var.mongodb_auto_scaling_compute_enabled
  auto_scaling_min_instances   = var.mongodb_auto_scaling_min_instances
  auto_scaling_max_instances   = var.mongodb_auto_scaling_max_instances
  backup_retention_period      = var.mongodb_backup_retention_period

  vpc_id           = module.vpc.vpc_id
  vpc_cidr_block   = module.vpc.vpc_cidr_block
  atlas_cidr_block = var.mongodb_atlas_cidr_block
  aws_account_id   = data.aws_caller_identity.current.account_id
  route_table_id   = module.vpc.private_route_table_ids[0]
}

output "mongodb" {
  value     = module.mongodb
  sensitive = true
}

####################################################################################################
## AWS RDS MySQL
####################################################################################################

module "mysql" {
  source         = "./mysql"
  cluster_name   = var.cluster_name
  rds_subnet_ids = concat(module.vpc.private_subnets, module.vpc.public_subnets)
  vpc_id         = module.vpc.vpc_id

  rds_instance_class          = var.rds_instance_class
  rds_mysql_version           = var.rds_mysql_version
  rds_min_storage             = var.rds_min_storage
  rds_max_storage             = var.rds_max_storage
  rds_backup_retention_period = var.rds_backup_retention_period
  rds_storage_encrypted       = var.rds_storage_encrypted

  ingress_cidr_blocks = ["10.0.0.0/8"]

  default_tags = local.default_tags
}

output "mysql" {
  value     = module.mysql
  sensitive = true
}
