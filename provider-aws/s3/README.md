# S3

This module provisions AWS S3 buckets for the cluster to store the tutor env and for
the instances and provides the credentials that are passed on to Tutor as configuration.

## External Dependencies

The host where this module is being provisioned from must have `awscli`
installed, and AWS access keys configured.

## Required Variables

- `bucket_prefix`: This will be used as the prefix for the bucket name
- `tags`: Tags for attaching to the buckets for identification later

## Outputs

- `edxapp_s3_bucket` - This contains the output of the `aws_s3_bucket` Terrafrom module.
- `edxapp_s3_user_access_key` - This contains the output of `aws_iam_access_key` Terraform module for the IAM user who has access to the bucket.