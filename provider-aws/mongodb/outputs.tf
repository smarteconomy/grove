output "connection_string" {
  value = mongodbatlas_cluster.cluster.connection_strings[0].standard
}

output "instances" {
  sensitive = true
  value = {
    for instance in var.tutor_instances :
    instance => {
      database       = instance
      forum_database = "${instance}-cs_comments_service"
      username       = instance
      password       = mongodbatlas_database_user.users[instance].password
    }
  }
}
