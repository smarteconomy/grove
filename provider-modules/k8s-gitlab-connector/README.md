# k8s-gitlab-connector

This module connects a Kubernetes cluster with a GitLab project.

# Variables
- `gitlab_cluster_agent_token`: The cluster agent token for the repository obtained from Gitlab.

# Output

This module doesn't provide any output.
