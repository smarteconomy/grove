# Instance Lifecycles

This section of the documentation describes what lifecycles an instance has and how you can reach the desired lifecycle for an instance.

Grove allows you to manage instances either [locally](./user-guides/working-locally.md) or through GitLab CI pipelines. To operate the Kubernetes cluster and instances running on it properly, it helps to be aware of the effect of your actions on the cluster's health.

## Instance states

Instances can have many states and the state is changed by the CI pipeline triggers -- in most of the cases. Before delving further, consider the diagram below.

```mermaid
stateDiagram-v2
    state "No configuration" as noConfig
    state "configuration created" as newConfig

    state instanceProvisioning <<choice>>
    state "Instance provisioned" as instanceProvisionSuccess
    state "Instance provisioning failed" as instanceProvisionFailure

    state instanceDeploying <<choice>>
    state "Instance deployed" as instanceDeploySuccess
    state "Instance deployment failed" as instanceDeployFailure

    state instanceDeprovisioning <<choice>>
    state "Instance deprovisioned" as instanceDeprovisionSuccess
    state "Instance deprovisioning failed" as instanceDeprivisionFailure

    %% Provision a new instance
    [*] --> noConfig
    noConfig --> instanceProvisioning: create instance pipeline triggered
    instanceProvisioning --> instanceProvisionFailure
    instanceProvisioning --> instanceProvisionSuccess
    instanceProvisionFailure --> [*]
    instanceProvisionSuccess --> newConfig: merge request created and merged
    
    %% Deploy a provisioned instance
    newConfig --> instanceDeploying: update instance pipeline triggered
    instanceDeploying --> instanceDeployFailure
    instanceDeployFailure --> [*]
    instanceDeploying --> instanceDeploySuccess

    %% Archive instance
    instanceDeploySuccess --> instanceDeploying: archive instance pipeline triggered
    instanceDeployFailure --> instanceDeploying: archive instance pipeline triggered

    %% Deprovision a deployed instance
    instanceDeploySuccess --> instanceDeprovisioning: archive instance pipeline triggered
    instanceDeployFailure --> instanceDeprovisioning: archive instance pipeline triggered
    instanceDeprovisioning --> instanceDeprivisionFailure
    instanceDeprivisionFailure --> [*]
    instanceDeprovisioning --> instanceDeprovisionSuccess
    instanceDeprovisionSuccess --> noConfig: merge request created and merged
```

### State: No configuration

The configuration file does not exist yet. This may happen because we never created it, or it has been removed. This is the base state for an instance. We have no resources in the infrastructure for this instance and the configuration is not available yet.

### State: Instance provisioned

The instance provisioned state can be achieved in two ways:

1. Call the [create instance pipeline](./pipelines/create-instance.md), or
2. [Create an instance locally](./user-guides/deploying-instances.md#locally) and push its configuration to the repository

Either way the instance will be provisioned and the configuration will be available online.

!!! note

    When creating an instance locally, you jump to the "Configuration created" state, skipping the automatic merge request creation and merge steps. Which is not an issue in this case.

### State: Instance provisioning failed

When an instance provisioning encounters an issue, the provisioning fails it may leave resources behind that require manual cleanup, depending on the state after provisioning failed. For example, the merge request may remain open and now it needs to be merged or closed manually.

### State: Configuration created

Once an instance reaches this state, the configuration is available in the git repository and the instance is ready for deployment. The deployment involves infrastructure resource creation too.

### State: Instance deployed

When a git commit with the appropriate commit message is pushed, an [update instance pipeline](./pipelines/update-instance.md) is triggered. The pipeline applies the infrastructure modifications, and starts a deployment for the instance.

!!! note

    The initial instance deployment takes significantly more time than the deployments afterwards.

### State: Instance deployment failed

When an instance deployment encounters an issue, the deployment fails it may leave resources behind that requires manual cleanup. Although, under normal circumstances, it is unlikely to happen, this state requires manual intervention.

### State: Instance Archived

When an instance is archived, the configuration and resources are still in place, but the namespace is removed.

### State: Instance Archive failed

Since instance archive pipeline is an instance update pipeline, with automated configuration change, the failure scenario is the same as for the update pipeline.

### State: Instance deprovisioned

Instance deprovisioning, similarly to provisioning, can result in two states: success or failure. An instance deprovisioning removes the configuration and the attached resources. This change is final and cannot be undone as any data stored your databases will be destroyed. It is possible to restore this data could be restored from a backup, but it might not be up to date.

!!! warning

    Only call the [instance archive pipeline](./pipelines/archive-instance.md) if you know the instance is not needed anymore.

### State: Instance deprovisioning failed

This state is reached when the deprovisioning fails. The CI pipelines may or may not remove resources, so manual intervention is required.  
