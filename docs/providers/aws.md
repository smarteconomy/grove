# AWS

Grove AWS support is a work in progress, and we are actively developing it.

## AWS Resources

Grove uses the following AWS resources for Open edX deployment:

### Amazon EKS

Grove uses a managed EKS cluster to deploy Tutor instances in Kubernetes.

### S3 Storage

Grove uses separate S3 storage for the Tutor env directory for each Open edX instance.

### AWS RDS for MySQL

Grove uses AWS RDS as the MySQL database for each Open edX instance. Grove creates a root credential using terraform and Tutor uses that to create instance-specific user and database. The database name is generated from the instance name, replacing all hyphens (`-`) with underscores (`_`). And then adding `_openedx` at the end. For example, database name for `test-instance` will be `test_instance_openedx`. Due to this, there can't be two instances with names `test-instance` and `test_instance` in the cluster. In case the length of the database name is longer than 64 characters, it will take the first 64 characters as the database name (max length of MySQL database name can be 64 characters).

### AWS Load balancer

Grove uses [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/). This ingress controller creates an AWS Load Balancer resource, which will be used to route network requests for all instances in the cluster to the ingress controller.

### MongoDB Atlas

Grove uses a shared MongoDB Atlas cluster, hosted in AWS, for the Open edX instances. Each instance has read-write access to a database with the same name as the grove instance, as well as a dedicated discussion forum database, named as `{instance}-cs_comments_service`.

#### Setting up a MongoDB Atlas

In order to use MongoDB Atlas in Grove you need to manually set up the project and create
an API key.

##### Create a new project

Navigate to your organization's **Projects** page and click on `New Project`.

![MongoDB Atlas Projects List](images/mongo-projects.png "MongoDB Atlas Projects List")

You'll be presented with the below, where you can name your project.

![MongoDB Atlast New Project](images/mongo-create-project.png "MongoDB Atlast New Project" )

Once the project is created, you can find the Project ID in the URL or by visiting the Project settings.

##### Add the IPs to whitelist in the Network Access section

Under the **Network Access** section, you can add the IP addresses that need to be whitelisted. If you're using Grove locally, be sure to whitelist your current IP.

In the case you cannot restrict to specific IPs, you need to add `0.0.0.0/1` and `128.0.0.0/1`.

![MongoDB Add IP Address](images/mongo-network-access.png "MongoDB Add IP Address" )

##### Create API Keys

To create API keys, navigate to the **Access Manager** tab in the project's settings.

![Project Access Manager](images/api-keys.png "Project Access Manager" )

From here you can **Create an API Key**, with `Project Owner` permissions.

![Project Access Manager](images/api-key-information.png "Project Access Manager" )

Once created, you will receive Public and Private API keys. Copy these values into your Gitlab CI settings using the following keys. Please note that these values should be set in `private.yml` later, if you use Grove locally.

- `MONGODB_ATLAS_PRIVATE_KEY`
- `MONGODB_ATLAS_PUBLIC_KEY`

Add the whitelisted IPs to this section as well.

![API Key Access](images/api-key-access-list.png "API Key Access" )
