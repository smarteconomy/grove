# Overriding Configuration

Grove is built for managing multiple Open edX instances in a Kubernetes cluster. It uses Tutor for deploying those instances. Therefore Grove needs to support instance-level customization. Here we will talk about those customization features.

## Default Configurations

[Grove Template](https://gitlab.com/opencraft/dev/grove-template) repository has a [`defaults`](https://gitlab.com/opencraft/dev/grove-template/-/tree/main/defaults) directory, in which we specify a set of default configurations. Whenever Grove creates a new instance, it copies these configuration files to the `instances/<INSTANCE_NAME>/` directory.

The `defaults` directory contains the following files:

- `config.yml` - A default set of configurations for [Tutor](https://docs.tutor.overhang.io/configuration.html). Tutor will use this file to deploy instances.
- `grove.yml` - Configuration file used by Grove. More details can be found below.
- `defaults/plugins/grove-override.yml`: A default [Tutor YML plugin](https://docs.tutor.overhang.io/plugins/v0/gettingstarted.html#yaml-file). This plugin will be installed automatically. This will help us specify Open edX configuration in form of Tutor patches. To know more about patches visit [Tutor docs](https://docs.tutor.overhang.io/plugins/api.html?highlight=patch#patches).

Note that content of the `defaults` directory is only copied while deploying a new instance via Gitlab Trigger. A "new deployment" means that there is no `instances/<INSTANCE_NAME>` directory. Creating a new instance by running the `./control/tutor` command locally will not copy the `defaults` folder. We need to manually copy the `defaults` directory in that case. Run the following command to create a new instance locally:

```bash
cp -r ./defaults/ ./instances/<INSTANCE_NAME> && ./control/tutor.sh <INSTANCE_NAME> config save
```

Where `<INSTANCE_NAME>` is the new instance's name.

## Grove Configurations

Grove provides some useful features via `grove.yml`.

### Installing a custom theme

By default, Grove installs [`edx-simple-theme`](https://github.com/open-craft/edx-simple-theme), which is an easy-to-customize Open edX comprehensive theme. But we are not limited to using it. Grove has the following configuration in `grove.yml` to install a custom theme via Tutor.

- `COMPREHENSIVE_THEME_NAME`: The name of the theme. Used as theme name in LMS, CMS, and as the clone folder name. Defaults to `edx-simple-theme`.
- `COMPREHENSIVE_THEME_SOURCE_REPO`: The Git repository for the theme. Defaults to `https://github.com/open-craft/edx-simple-theme.git`.
- `COMPREHENSIVE_THEME_VERSION`: Branch for the theme. Defaults to `master`.
- `ENABLE_SIMPLE_THEME_SCSS_OVERRIDES`: Whether we want to override SCSS variables for the theme. Only works with [open-craft/edx-simple-theme](https://github.com/open-craft/edx-simple-theme.git).
- `SIMPLE_THEME_SCSS_OVERRIDES`: A list of [supported](https://github.com/open-craft/edx-simple-theme#list-of-available-sass-variables-for-customization) SCSS variable names and their values. Only works with [open-craft/edx-simple-theme](https://github.com/open-craft/edx-simple-theme.git). Example:

  ```yml
  SIMPLE_THEME_SCSS_OVERRIDES: 
    - value: '#0075b4'
      variable: footer-bg
    - value: '#6d9cae'
      variable: footer-bg
  ```

- `SIMPLE_THEME_STATIC_FILES_URLS`: A list of static file URLs and their destination path. Each static file is downloaded from the URL and saved in the destination path within `edx-simple-theme` in the tutor env directory. This is used to override various default static images in the Open edX platform such as favicon, header/footer icons, banner images, etc. Example:

  ```yml
  SIMPLE_THEME_STATIC_FILES_URLS:
    - dest: lms/static/images/logo.png
      url: <LOGO_URL>
    - dest: lms/static/images/favicon.ico
      url: <FAVICON_URL>
  ```

### Image build configuration

Any change in theming, installing XBlock, or custom Django app requires rebuilding the instance image. But we would not want to build all the images (e.g. forum) every time. Grove provides `IMAGE_BUILD_CONFIG` to specify which images to rebuild for an instance. It is a map where each key is an image name. For each image name, we need to specify `DOCKER_IMAGE_VAR` - the name of the variable Tutor uses to specify image name in the docker registry (in the case of Grove the GitLab container registry).

For example the value of `DOCKER_IMAGE_VAR` for the `openedx` image is [`TUTOR_DOCKER_IMAGE_OPENEDX`](https://docs.tutor.overhang.io/configuration.html#custom-images). For the `ecommerce` image, it is [`TUTOR_ECOMMERCE_DOCKER_IMAGE`](https://github.com/overhangio/tutor-ecommerce#configuration). `TUTOR_` prefix is added as we are going to set these variables in the build environment.

## Instance level customization

Once we've created/deployed a new instance we can now freely modify the instance-specific configuration in the `instances/<INSTANCE_NAME>` directory.

### Overriding files in Tutor env directory

Grove stores the Tutor `env` directory of each instance in a separate s3 storage. Since that directory is not tracked by Git, it's hard to maintain any changes that are applied to its items. To solve this issue, Grove provides an easy way to override or add any file to the Tutor `env` directory.

We've using `instances/<INSTANCE_NAME>/env-overrides` directory for this purpose. Any file kept there will be rendered to the `env` directory, thus overriding an existing file that matches its path. To render these files Grove uses [Tutor's `render`](https://docs.tutor.overhang.io/reference/cli/config.html#tutor-config-render) command.

For example, to [install](https://docs.tutor.overhang.io/configuration.html#installing-extra-xblocks-and-requirements) a custom XBlock we need to put the pip requirement in the `env/build/openedx/requirements/private.txt` file. Therefore, we put it in the `env-overrides/build/openedx/requirements/private.txt` file and it will be rendered to `env/build/openedx/requirements/private.txt` by Grove.

### Custom Tutor Plugin

If we need to add another YML plugin for Tutor we can just create `instances/<INSTANCE_NAME>/plugins/<PLUGIN_NAME>.yml` file. And then add that to `instances/<INSTANCE_NAME>/config.yml` to install it.

```yml
PLUGINS:
  ... other plugins
  - <PLUGIN_NAME>
```

### Site Configuration

Site configurations can be added for your instance using the `SITE_CONFIG` configuration in `instances/<INSTANCE_NAME>/config.yml`
This can either be configured directly by modifying the config file or by passing the json using `TUTOR_SITE_CONFIG` key with the deployment request. Example:

```yml
SITE_CONFIG:
  homepage_overlay_html: >-
    <h1>
    Welcome to the customized instance
    </h1>
    <p>
    This banner is customized using the site configurations
    </p>
  static_template_about_content: <p>This is a custom about page</p>
  version: 0
```
