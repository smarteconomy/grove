# Debugging Instances

Using Grove means using a Kubernetes cluster. For security considerations, the cluster is put into a VPC, and the resources within this VPC are not directly accessible via the internet. Only the Nginx ingress controller receives traffic and routes it to the applicable service.

Since Grove is not providing director instances/jump hosts, it is not possible to ssh into the instances directly -- as it was done with OpenStack VMs --, thus it is required to use the `./control/kubectl` control command.

## Connecting to instances

First of all, you have to have the repository setup properly for local use. To do so, follow the [working locally](./working-locally.md) guide's "Prerequisites" section, and make sure to remove any `kubeconfig-private.yml` file from the repository root.

1. When the local setup is configured, change directory to `./control`.
1. Run `./kubectl get namespaces` to find the desired namespace. Since all resources are in a dedicated namespace, we need to know which namespace we want to use.
1. Run `./kubectl -n <NAMESPACE> get pods` to get the pods running in the given namespace.
1. Select the pod you would like to connect to, and run `./kubectl -n <NAMESPACE> exec -it <POD_NAME> bash`.

At this point, you are connected to a pod and you can debug the instance as you would do with VMs.
