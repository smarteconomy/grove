# Commit Based Pipelines

Grove is a set of GitLab CI pipelines, bootstrapping scripts, and wrappers that make it easy to deploy Open edX via [Tutor](https://docs.tutor.overhang.io/index.html). Depending on the use case, these pipelines may be triggered by pipeline trigger API calls, schedules, or git flows, but they are common in one thing: pipelines are generated based on the given commit messages.  

Within the pipeline definition, we are generating CI/CD child jobs based on commit message patterns. This section of the documentation describes these pipelines in more detail.
