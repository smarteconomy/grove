# Create Instance Pipeline

Create instance pipeline triggers a new instance creation.

## Use case

Instance creation can be done [locally](../user-guides/working-locally), though that requires more setup and depends on the user's machine. To avoid this dependency and fully automate instance creation, you can use the `[AutoDeploy][Create] ...` commit message.

## Commit message

The following commit message pattern is used to trigger an instance creation for a single instance.

`[AutoDeploy][Create] <INSTANCE NAME>|<DEPLOYMENT ID>"`

To create multiple instances with a single commit message, list the `<INSTANCE NAME>|<DEPLOYMENT ID>` pairs, separated by comma.

`[AutoDeploy][Create] <INSTANCE NAME>|<DEPLOYMENT ID>,<INSTANCE NAME>|<DEPLYOMENT ID>,..."`

The arguments of the pipeline:

* `<INSTANCE NAME>` - The name of the instance to create
* `<DEPLOYMENT ID>` - ID to indicate the current deployment (in most cases, you will use `1` for instance creation)

Example commit message:

`[AutoDeploy][Create] opencraft-courses|2`

!!! note

    Instance names are slugified when parsing instance name. If the slugified instance name exists, the pipeline will fail.

!!! warning

    The create and corresponding archive pipelines are special pipelines that are triggered by a script and requires additional CI variable existence. The expected way of calling the create and archive pipelines is calling a GitLab pipeline trigger. 

    In order to let the CI create the instance config and commit the changes, set the following at pipeline run, or use pipeline trigger parameters:
    
    - `NEW_INSTANCE_TRIGGER` to any value
    - `INSTANCE_NAME` to the name of the instance
    - `DEPLOYMENT_REQUEST_ID` to the numeric deployment ID
