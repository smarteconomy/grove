# Update Instance Pipeline

Update instance pipeline triggers an instance update.

## Use case

The most common use case is updating the instance's configuration or apply patches on the container image.

## Commit message

The following commit message pattern is used to trigger an instance update for a single instance.

`[AutoDeploy][Update] <INSTANCE NAME>|<DEPLOYMENT ID>"`

To update multiple instances with a single commit message, list the `<INSTANCE NAME>|<DEPLOYMENT ID>` pairs, separated by comma.

`[AutoDeploy][Update] <INSTANCE NAME>|<DEPLOYMENT ID>,<INSTANCE NAME>|<DEPLYOMENT ID>,..."`

The arguments of the pipeline:

* `<INSTANCE NAME>` - The name of the instance to update
* `<DEPLOYMENT ID>` - ID to indicate the current deployment

Example commit message:

`[AutoDeploy][Update] opencraft-courses|2`
