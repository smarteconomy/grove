# Batch Upgrade Pipeline

Batch upgrade pipeline triggers batch upgrade on one more servers.

## Use case

Build images for all instances that match with the given `<EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>` in the cluster and redeploys them. These `EDX_PLATFORM_REPOSITORY` and `EDX_PLATFORM_VERSION` will be matched against the instance's `config.yml` value. It will first upgrade base images and then upgrade the instances.

## Commit message

The following commit message pattern is used to trigger a batch upgrade.

`[AutoDeploy][Batch][Upgrade] <EDX_PLATFORM_REPOSITORY>|<EDX_PLATFORM_VERSION>`

The arguments of the pipeline:

* `<EDX_PLATFORM_REPOSITORY>` - Git repository URL
* `<EDX_PLATFORM_VERSION>` - Git branch, tag, commit hash that Grove and Tutor checks out during image build

Example commit message:

`[AutoDeploy][Batch][Upgrade] https://github.com/openedx/edx-platform|maple.3`
