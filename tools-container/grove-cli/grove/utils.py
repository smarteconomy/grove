from __future__ import annotations

import json
import os
import re
import subprocess
from typing import List, Optional
from urllib.parse import urlparse

import typer
from slugify import slugify

from grove.const import CI_REGISTRY_IMAGE, SCRIPTS_DIR
from grove.exceptions import GroveError
from grove.type_defs import InstanceCustomize

SYNC_SCRIPT_PATH = SCRIPTS_DIR / "sync_tutor_env.sh"
SYNC_SCRIPT = f"bash {SYNC_SCRIPT_PATH}"


def slugify_instance_name(name: str) -> str:
    """
    Slugify the instance name and return it.
    """
    return slugify(name)


def instance_customization_from_trigger_payload() -> InstanceCustomize:
    """
    While running from CI Trigger, the trigger payload is available in $TRIGGER_PAYLOAD as a file.
    This function reads that file as JSON and preplares InstanceCustomize.
    """
    payload_file = os.environ.get("TRIGGER_PAYLOAD")
    if payload_file:
        with open(payload_file) as file:
            payload = json.load(file)

        result = InstanceCustomize()
        result["grove"] = {}
        result["tutor"] = {}
        result["env"] = {}
        tutor_prefix = "TUTOR_"
        grove_prefix = "GROVE_"
        env_prefix = "ENV_"
        for key, value in payload.items():
            if key.startswith(tutor_prefix):
                key = key.replace(tutor_prefix, "")
                result["tutor"][key] = value
            elif key.startswith(grove_prefix):
                key = key.replace(grove_prefix, "")
                result["grove"][key] = value
            elif key.startswith(env_prefix):
                key = key.replace(env_prefix, "")
                result["env"][key] = value
        return result
    else:
        return {}


def execute(command: str, env: dict = {}, hide_output=False, capture_output=False) -> Optional[str]:
    """
    An utility to run shell command

    Args:
        command (str) - command to run in shell.
        env (dict) - shell env variables in key value format.
        hide_output (bool) - defaults to False. If True output will not be printed to console.
        capture_output (bool) - defaults to False. If True output will be returned as string.
    Returns:
        str if capture_output is True else None
    """
    typer.echo(command)
    environ = os.environ.copy()

    # override environ with provided env variables
    for key, val in env.items():
        environ[key] = str(val)  # make sure all values are string

    # placeholder for capturing output
    output = []

    with subprocess.Popen(
        command,
        env=environ,
        stdout=subprocess.PIPE,
        shell=True,
        universal_newlines=True,
    ) as process:
        try:
            for line in iter(process.stdout.readline, ""):
                if capture_output:
                    output.append(line)
                if not hide_output:
                    typer.echo(line)
            result = process.wait(timeout=None)
        except KeyboardInterrupt:
            process.kill()
            process.wait()
            raise
        except Exception as e:
            process.kill()
            process.wait()
            raise GroveError(f"Command failed: {command}") from e
        if result > 0:
            raise GroveError("Command failed with status {}: {}".format(result, command))

    if capture_output:
        return "\n".join(output)
    else:
        return None


def tutor_env_pull(env: dict):
    execute(f"{SYNC_SCRIPT} pull", env)


def tutor_env_push(env: dict):
    execute(f"{SYNC_SCRIPT} push", env)


class RepoRelease:
    """
    An utility class to represent a repository and release.

    This exists because repository url can be of following formats -
        - git@github.com:open-craft/edx-platform.git
        - https://github.com/open-craft/edx-platform.git
        - http://github.com/open-craft/edx-platform.git

    This class will treat as all of these are the same.
    """

    def __init__(self, repo: str, release: str) -> None:
        self.repo = repo
        self.release = release

        if self.repo.startswith("git@"):
            self.hostname, self.repo_path = self._parse_git_url()
        else:
            self.hostname, self.repo_path = self._parse_url()

    def _parse_git_url(self):
        """
        Parse url with Git Format
        ex: git@github.com:open-craft/edx-platform.git

        Returns:
            hostname (str) - ex: github.com
            path (str) - ex: open-craft/edx-platform
        """
        netaddr, path = self.repo.split(":")
        hostname = netaddr.split("@")[1]
        return hostname, path.strip("/").rstrip(".git")

    def _parse_url(self):
        """
        Parse http and htts urls

        Returns:
            hostname (str) - ex: github.com
            path (str) - ex: open-craft/edx-platform
        """
        parsed = urlparse(self.repo)
        return parsed.hostname, parsed.path.strip("/").rstrip(".git")

    def equals(self, rr: RepoRelease) -> bool:
        """
        Checks if another instance of RepoRelease is the same as current one.

        Args:
            rr (RepoRelease)

        Returns:
            bool - True if both are same, false otherwise
        """
        return self.hostname == rr.hostname and self.repo_path == rr.repo_path and self.release == rr.release

    def get_tagged_base_image(self) -> str:
        """
        Generate base image tag for this repo and release.
        """
        tag = f"{self.hostname}-{self.repo_path}-{self.release}"
        tag = re.sub(r"[./-]", "_", tag)
        return f"{CI_REGISTRY_IMAGE}/base_images/openedx:{tag}"

    def is_available(self, available_releases: List[RepoRelease]) -> bool:
        """
        Checks if current RepoRelease is included in the provided list.
        """
        for repo_release in available_releases:
            if self.equals(repo_release):
                return True
        return False

    def __str__(self) -> str:
        return f"<{self.repo}|{self.release}|{self.hostname}|{self.repo_path}>"
