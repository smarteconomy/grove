import json
import os
import re
import shutil
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Optional, Tuple
from urllib.error import URLError
from urllib.request import urlopen

import typer
import yaml
from tutor.config import load as tutor_config_loader
from tutor.env import Renderer as TutorEnvRenderer

from grove import terraform
from grove.const import CI_REGISTRY_IMAGE, GROVE_DEFAULTS_DIR, INSTANCES_DIR, Plugin
from grove.exceptions import GroveError
from grove.monitoring import Monitoring
from grove.type_defs import GroveConfig, InstanceCustomize, TutorEnv
from grove.utils import (
    RepoRelease,
    execute,
    slugify_instance_name,
    tutor_env_pull,
    tutor_env_push,
)

MONITORED_HOSTS = ("LMS_HOST", "CMS_HOST", "PREVIEW_LMS_HOST")


class Instance(Plugin):
    """
    This class represents a single Open edX instance in Grove.
    """

    def __init__(self, instance_name: str) -> None:
        """
        Initialize a instance with instance_name.
        """
        # name of the instance
        self.instance_name: str = slugify_instance_name(instance_name)

        # instance root directory
        self.root_dir: Path = INSTANCES_DIR / self.instance_name

        # instance tutor env directory
        self.env_dir: Path = self.root_dir / "env"

        # instance tutor env-overrides directory
        self.env_override_dir: Path = self.root_dir / "env-overrides"

        # grove.yml file path
        self.grove_config_file: Path = self.root_dir / "grove.yml"

        # config.yml file path
        self.tutor_config_file: Path = self.root_dir / "config.yml"

        # root directory for placing custom theme
        self.edxapp_theme_root_dir: Path = self.env_dir / "build" / "openedx" / "themes"

        # MFE directory for placing custom theme
        self.mfe_theme_root_dir: Path = self.env_dir / "plugins" / "mfe" / "build" / "mfe" / "themes"

        # grove.yml file loaded as dictionary
        self._grove_config: GroveConfig = None

        # config.yml file loaded as dictionary
        self._tutor_config: Optional[dict] = None

        # groveoverride tutor plugin loaded as dictionary
        self._plugin_config: dict = dict()

    def tutor_env_sync(self):
        """
        Utility method to sync tutor env directory with s3.
            - Pull from s3
            - runs `tutor config save`
            - renders `env-overrides` dir
            - Push to s3
        """
        self.tutor_env_pull()
        self.tutor_save_config()
        self.tutor_env_override()
        self.tutor_env_push()

    def tutor_save_config(self, env: Optional[dict] = None):
        """
        Utility method to run `tutor config save` command
        """
        if env is None:
            env = {}

        self.tutor_command("config save", env=env)

    def load_instance_configs(self):
        """
        Load Grove and Tutor config for this instance.
        """
        with open(self.grove_config_file) as file:
            self._grove_config = yaml.safe_load(file)
        with open(self.tutor_config_file) as file:
            self._tutor_config = yaml.safe_load(file)

    def save_instance_configs(self):
        """
        Save Grove and Tutor config for this instance.
        """
        with open(self.grove_config_file, "w") as file:
            yaml.safe_dump(self._grove_config, file)
        with open(self.tutor_config_file, "w") as file:
            yaml.safe_dump(self._tutor_config, file)

    def apply_openedx_env_configs(self, config: dict):
        """
        Load the plugins file and save the openedx env configs
        """
        patches = self.plugin_config.get(self.PATCH_KEY, {})

        for config_name in config:
            patch_config = {}
            patch_config_key = self.PATCHES[config_name]

            if patches.get(patch_config_key):
                patch_config = yaml.load(patches.get(patch_config_key), loader=yaml.Loader)

            patch_config.update(config[config_name])

            # Convert the object into a pretty-printed json string, and then
            # strip the leading and trailing curly braces and add a new line
            patch_config_string = yaml.dump(patch_config).strip() + "\n"

            patches[patch_config_key] = patch_config_string

        self.plugin_config[self.PATCH_KEY] = patches

    @property
    def grove_config(self):
        if self._grove_config is None:
            self.load_instance_configs()
        return self._grove_config

    @property
    def tutor_config(self):
        if self._tutor_config is None:
            self.load_instance_configs()
        return self._tutor_config

    @property
    def plugin_config(self):
        if self._plugin_config is None:
            self.load_instance_configs()
        return self._plugin_config

    def exists(self) -> bool:
        """
        Check if this instance exists.
        """
        return Path.exists(self.root_dir)

    def is_name_valid(self) -> bool:
        """
        Return if the instance name is valid.

        The instance name is valid if the name is:
        - not managed by Grove, and
        - not a reserved K8s namespace, and
        - not starts with an illegal (other than a-z) character, and
        - not longer than 49 characters

        At this point, the instance name will be normalized, hence we don't
        need to check for that.

        The 49 characters limit is coming from that DigitalOcean cannot handle
        bucket names that have more than 63 characters, and we add 14 chars of
        pre- and suffix to the instance name for buckets.
        """
        restricted_names = {
            "default",
            "gitlab-kubernetes-agent",
            "kube-node-lease",
            "kube-public",
            "kube-system",
            "monitoring",
            "openfaas",
            "openfaas-fn",
        }

        registered_instance_names = {instance for instance in os.listdir(INSTANCES_DIR) if os.path.isdir(instance)}

        is_not_restricted = self.instance_name not in restricted_names
        is_not_registered = self.instance_name not in registered_instance_names
        starts_with_az = re.match(r"^[^a-z].*", self.instance_name) is None
        within_length_limit = 49 >= len(self.instance_name) >= 3

        return is_not_restricted and is_not_registered and starts_with_az and within_length_limit

    def tutor_default_env(self) -> TutorEnv:
        """
        Return common Tutor env for current instance.
        """
        return {
            "TUTOR_ID": self.instance_name,
            "TUTOR_ROOT": str(self.root_dir),
            "TUTOR_K8S_NAMESPACE": self.instance_name,
        }

    def tutor_env(self) -> Dict:
        """
        Build Tutor environment
        """
        env = self.tutor_default_env()
        env.update(self.instance_image_envs())
        if terraform.is_infrastructure_ready(self.instance_name):
            env.update(terraform.get_env(self.instance_name))
        else:
            typer.echo(f"Infrastructure is not ready yet for {self.instance_name}. Terraform state is not available.")
        return env

    def create(self):
        """
        Create current instance. Raises GroveError if instance is already created.

        Args:
            env (dict) - will be passed
        """
        if self.exists():
            raise GroveError(f"Instance {self.instance_name} already exists. Can't create.")

        if not self.is_name_valid():
            raise GroveError(f'Instance name "{self.instance_name}" is not valid. Can\'t create.')

        typer.echo(f"Copying {GROVE_DEFAULTS_DIR} to {self.root_dir}...")
        shutil.copytree(GROVE_DEFAULTS_DIR, self.root_dir)

    def prepare(self, customization: InstanceCustomize):
        """
        If current instance doesn't exist creates it.
        Then updates grove.yml and config.yml with given customization.
        Finally, runs `tutor config save`
        """
        if not self.exists():
            typer.echo(f"Instance {self.instance_name} doesn't exist, creating...")
            self.create()
            typer.echo(f"Instance {self.instance_name} created.")
        else:
            typer.echo(f"Instance {self.instance_name} already exists.")

        # apply customization
        self.customize(customization)

        self.tutor_save_config()

        # reload configurations
        self.load_instance_configs()

    def customize(self, customization: InstanceCustomize):
        """
        Given a customization, it will override and save grove.yml and config.yml for Tutor.

        Args:
            customization (InstanceCustomize)
        """
        typer.echo(f"Applying customization for {self.instance_name}.")
        self.load_instance_configs()

        self.grove_config.update(customization.get("grove", {}))
        self.tutor_config.update(customization.get("tutor", {}))
        self.tutor_config["ID"] = self.instance_name
        self.tutor_config["K8S_NAMESPACE"] = self.instance_name
        self.tutor_config[
            "MYSQL_ROOT_PASSWORD"
        ] = "to-be-set-via-environment"  # this is to avoid tutor to generate root mysql password.

        typer.echo(f"Applying env configs for {self.instance_name}")
        self.apply_openedx_env_configs(customization.get("env", {}))

        self.save_instance_configs()

    def delete(self):
        """
        Deletes an instance. Don't use this unless sure. Currently used only for testing.
        """
        typer.echo(f"Deleting {self.instance_name}!")
        shutil.rmtree(self.root_dir)

    def tutor_env_pull(self):
        """
        Pull env directory from s3
        """
        use_terraform = terraform.is_infrastructure_ready(self.instance_name)
        if use_terraform:
            tutor_env_pull(self.tutor_env())
        else:
            typer.echo(f"Unable to pull tutor env from s3 for {self.instance_name}. Infrastructure is not ready.")

    def tutor_env_push(self):
        """
        Push env directory to s3
        """
        use_terraform = terraform.is_infrastructure_ready(self.instance_name)
        if use_terraform:
            tutor_env_push(self.tutor_env())
        else:
            typer.echo(f"Unable to push tutor env to s3 for {self.instance_name}. Infrastructure is not ready.")

    def pre_deploy(self):
        """
        Prepare current instance for deployment.
        """
        if terraform.is_infrastructure_ready(self.instance_name):
            self.tutor_env_pull()
            self.tutor_save_config()
            self.tutor_env_override()
            self.prepare_theme()
            self.tutor_env_push()
        else:
            raise GroveError(f"Infrastructure is not ready for {self.instance_name}.")

    def prepare_theme(self):
        """
        Install custom theme for this instance
        """
        theme_name = self.grove_config.get("COMPREHENSIVE_THEME_NAME")
        theme_repo = self.grove_config.get("COMPREHENSIVE_THEME_SOURCE_REPO")
        theme_version = self.grove_config.get("COMPREHENSIVE_THEME_VERSION")
        theme_dir = self.edxapp_theme_root_dir / theme_name

        if Path(theme_dir).exists():
            shutil.rmtree(theme_dir)

        if all([theme_name, theme_repo, theme_version]):

            self.clone_theme_into_tutor_env(theme_version, theme_repo, theme_name)

            if self.grove_config.get("ENABLE_SIMPLE_THEME_SCSS_OVERRIDES"):
                typer.echo("Simple theme SCSS override is enabled.")

                scss_dir = theme_dir / "lms" / "static" / "sass"

                # a notice to append on top of the generated files
                autogenerated_notice = (
                    "// This file has been autogenerated by Grove.\n// Don't modify this file manually.\n"  # noqa: E501
                )

                self.generate_common_scss_overrides(scss_dir, autogenerated_notice)

                self.generate_lms_scss_overrides(scss_dir, autogenerated_notice)

                self.download_and_set_static_files(theme_dir)
            else:
                typer.echo("Simple theme SCSS override is disabled.")

            self.copy_theme_into_mfe_build_dir()
        else:
            typer.echo("No theme repository provided. Skiping installing theme.")

    def clone_theme_into_tutor_env(self, theme_version, theme_repo, theme_name):
        typer.echo(f"Cloning {theme_repo}#{theme_version} into {theme_name}")

        git_command = f"git clone --branch {theme_version} {theme_repo} {theme_name}"
        cd_command = f"cd {self.edxapp_theme_root_dir}"
        execute(f"{cd_command} && {git_command}")

    def copy_theme_into_mfe_build_dir(self):
        typer.echo("Copying theme files into build/mfe/themes folder.")

        execute(f"mkdir -p {self.mfe_theme_root_dir}")
        execute(f"cp -r {self.edxapp_theme_root_dir}/. {self.mfe_theme_root_dir}")

    def generate_common_scss_overrides(self, scss_dir, autogenerated_notice):
        scss_overrides = self.grove_config.get("SIMPLE_THEME_SCSS_OVERRIDES", [])
        # generate common-variables.scss file
        common_variables_file = scss_dir / "common-variables.scss"
        scss_content = "\n".join([f'${item["variable"]}: {item["value"]};\n' for item in scss_overrides])
        Path(common_variables_file).unlink(missing_ok=True)
        Path(common_variables_file).write_text(f"{autogenerated_notice}\n{scss_content}", encoding="utf-8")
        typer.echo("Generated common-variables.scss")

    def generate_lms_scss_overrides(self, scss_dir, autogenerated_notice):
        # generates _lms-overrides.scss
        lms_overrides_file = scss_dir / "_lms-overrides.scss"
        extra_scss = self.grove_config.get("SIMPLE_THEME_EXTRA_SCSS", "")
        Path(lms_overrides_file).unlink(missing_ok=True)
        Path(lms_overrides_file).write_text(
            f"{autogenerated_notice}@import 'common-variables';\n{extra_scss}",
            encoding="utf-8",
        )
        typer.echo("Generated _lms-overrides.scss")

    def download_and_set_static_files(self, theme_dir):
        """
        Download static files from their url and save them in the theme repo.
        To override the default static image files such as favicon, header/footer
        logo, banner image, etc. we need to have a corresponding image file in
        the correct directory of `edx-simple-theme`.
        This method downloads the static files from the given urls and saves them
        in the given file path inside the cloned copy of the edx-simple-theme in
        the tutor env directory which is then complied during the image build stage.
        """
        static_files = self.grove_config.get("SIMPLE_THEME_STATIC_FILES_URLS")
        if static_files:
            for static_file in static_files:
                dest = theme_dir / static_file["dest"]
                dest.parent.mkdir(parents=True, exist_ok=True)
                try:
                    response = urlopen(static_file["url"])
                    with open(dest, "wb") as file:
                        file.write(response.read())
                except URLError as e:
                    typer.echo(f"Error {e} while downloading file from url {static_file['url']}")

        typer.echo("Downloaded all static files")

    def tutor_env_override(self, env: Optional[dict] = None):
        """
        Render configs from env-overrides dir.
        """
        if env is None:
            env = {}

        if Path(self.env_dir).exists():
            if Path(self.env_override_dir).exists():
                env_overrides_dir = str(self.env_override_dir)
                env_dir = str(self.env_dir)

                config = tutor_config_loader(self.root_dir)
                renderer = TutorEnvRenderer(config)
                renderer.template_roots = [env_overrides_dir]
                renderer.render_all_to(env_dir)
            else:
                typer.echo(f"No env-overrides directory found for {self.instance_name}.")
        else:
            typer.echo(f"No env directory found for {self.instance_name}. Skipping env-overrides config rendering!")

    def tutor_command(self, argstr: str, env=None) -> str:
        """
        Run Tutor for this instance.
        """
        if env is None:
            env = {}

        tutor_env = self.tutor_env()
        tutor_env.update(env)
        return execute(f"tutor {argstr}", capture_output=True, env=tutor_env)

    def deploy(self):
        """
        Deploy this instance to Kubernetes.
        """
        if terraform.is_infrastructure_ready(self.instance_name):
            self.tutor_env_pull()
            self.tutor_save_config()
            self.tutor_env_override()
            self.tutor_command("k8s quickstart --non-interactive")
            self.tutor_env_push()
        else:
            raise GroveError(f"Infrastructure is not ready for {self.instance_name}.")

    def archive(self):
        """
        Remove instance namespace and all its resources, but keep the infrastructure
        components.
        """
        execute(f"kubectl delete namespace --ignore-not-found=true {self.instance_name}")

    def enable_theme(self):
        """
        Enable comprehensive theme. Instance have to be deployed before enabling theme.
        """
        theme_name = self.grove_config.get("COMPREHENSIVE_THEME_NAME")
        self.tutor_command(
            f"k8s settheme {theme_name} -d $(tutor config printvalue LMS_HOST) -d $(tutor config printvalue CMS_HOST)"
        )

    def force_update_k8s_images(self):
        """
        After deployment we need to update docker images in k8s.
        """
        # updating docker images in kubernetes
        # https://docs.tutor.overhang.io/k8s.html#updating-docker-images
        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        k8s_patch = {"spec": {"template": {"metadata": {"labels": {"date": now}}}}}
        patch_str = json.dumps(k8s_patch).replace('"', '\\"')

        # patch only deployments. Patching jobs raises an error.
        get_deployment_names = f"kubectl get deployments -o name -n {self.instance_name}"

        parse_and_patch = "sed -e 's/.*\\///g' | xargs -I {} kubectl patch deployment {} --patch "  # noqa: W605
        execute(f'{get_deployment_names} | {parse_and_patch} "{patch_str}" -n {self.instance_name}')

    def post_deploy(self):
        """
        Post deployment steps.
        """
        self.enable_theme()
        self.force_update_k8s_images()
        self.enable_new_relic_monitoring()

    def instance_images(self) -> List[Tuple[str, Dict]]:
        """
                Get list of images and their build config for a given instance.
        `
                Returns:
                    List[Tuple[str, Dict]] - (image name, build config)
        """
        return [
            (image_name, build_config)
            for image_name, build_config in self.grove_config.get("IMAGE_BUILD_CONFIG", {}).items()
        ]

    def instance_image_envs(self) -> Dict:
        """
        Return custom docker image related environment variables for Tutor.
        """
        env = {}
        for image_name, build_config in self.instance_images():
            env[build_config["DOCKER_IMAGE_VAR"]] = f"{CI_REGISTRY_IMAGE}/{self.instance_name}/{image_name}:latest"
        return env

    def edx_platform_repo_release(self):
        """
        Returns RepoRelease instance for LMS & CMS

        Returns:
            RepoRelease
        """
        return RepoRelease(
            self.tutor_config["EDX_PLATFORM_REPOSITORY"],
            self.tutor_config["EDX_PLATFORM_VERSION"],
        )

    def batch_upgrade_enabled(self):
        return not self.grove_config.get("BATCH_UPGRADE_DISABLED", False)

    def enable_new_relic_monitoring(self):
        """
        Enables new relic monitoring if the environment variables are set
        correctly.
        """
        api_key = os.environ.get("NEW_RELIC_API_KEY")
        if not api_key:
            typer.echo("Skipping monitoring, NEW_RELIC_API_KEY is empty.")
            return None

        emails = os.environ.get("NEW_RELIC_MONITORING_EMAILS")
        if not emails:
            typer.echo("Skipping monitoring, NEW_RELIC_MONITORING_EMAILS empty.")
            return None
        emails = [email.strip() for email in emails.split(",")]
        region = os.environ.get("NEW_RELIC_REGION_CODE")

        if not region:
            typer.echo("Skipping monitoring, NEW_RELIC_REGION_CODE is empty.")
            return None

        urls = self._get_monitoring_urls()

        monitoring = Monitoring(emails, urls, self.instance_name, api_key, region)
        return monitoring.enable_monitoring()

    def _get_monitoring_urls(self):
        urls = []
        for host_key in MONITORED_HOSTS:
            domain = self.tutor_config.get(host_key)
            if not domain:
                continue
            urls.append(f"https://{domain}/")

        lms_domain = self.tutor_config.get("LMS_HOST")
        if lms_domain:
            heartbeat_url = f"https://{lms_domain}/heartbeat?extended"
            urls.append(heartbeat_url)

        return urls


def load_all_instances() -> List[Instance]:
    """
    Load all available instances.
    """
    return [Instance(name) for name in os.listdir(INSTANCES_DIR) if os.path.isdir(f"{INSTANCES_DIR}/{name}")]


def load_all_repo_releases() -> List[RepoRelease]:
    """
    Load all available RepoReleases.
    """
    return [instance.edx_platform_repo_release() for instance in load_all_instances()]
