import os
from pathlib import Path

WORKSPACE_DIR = Path(os.environ.get("CI_PROJECT_DIR", "/workspace"))
INSTANCES_DIR = WORKSPACE_DIR / "instances"
GROVE_DEFAULTS_DIR = WORKSPACE_DIR / "defaults"
GROVE_TEMPLATES_DIR = Path(__file__).parent.absolute() / "templates"
SCRIPTS_DIR = Path(__file__).parent.parent.parent.absolute() / "scripts"

CI_REGISTRY_IMAGE = os.getenv("CI_REGISTRY_IMAGE")


class Plugin:
    PATCH_KEY = "patches"
    PATCHES = {
        "CMS": "cms-env",
        "CMS_FEATURES": "cms-env-features",
        "COMMON_FEATURES": "common-env-features",
        "LMS": "lms-env",
        "LMS_FEATURES": "lms-env-features",
    }
