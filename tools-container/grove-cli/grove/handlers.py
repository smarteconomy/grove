"""
Trigger Handlers
"""

import typer

from grove.pipeline import DummyPipeline, batch_update_pipeline
from grove.utils import RepoRelease


class WebhookData:
    def __init__(self, payload: dict):
        self.edx_repository = None
        self.branch_name = None
        self.merged = False
        self.payload = payload
        self.parse()

    def parse(self):
        raise NotImplementedError()

    def __str__(self) -> str:
        return f"<{self.edx_repository}|{self.branch_name}|{self.merged}>"


class GitHubWebhookData(WebhookData):
    def parse(self):
        self.edx_repository = self.payload.get("pull_request", {}).get("base", {}).get("repo", {}).get("clone_url")
        self.branch_name = self.payload.get("pull_request", {}).get("base", {}).get("ref")
        self.merged = all(
            [
                self.payload.get("action") == "closed",
                self.payload.get("pull_request", {}).get("merged", False),
                self.edx_repository is not None,
                self.branch_name is not None,
            ]
        )


class GitLabWebhookData(WebhookData):
    def parse(self):
        if self.payload.get("object_kind") == "merge_request" and self.payload.get("event_type") == "merge_request":
            self.edx_repository = self.payload.get("repository", {}).get("url")
            self.branch_name = self.payload.get("object_attributes", {}).get("target_branch")
            self.merged = all(
                [
                    self.payload.get("object_attributes", {}).get("action") == "merge",
                    self.edx_repository is not None,
                    self.branch_name is not None,
                ]
            )


def base_image_upgrade_handler(payload: dict):
    typer.echo("Parsing following payload -")
    typer.echo(payload)

    if payload.get("pull_request"):
        typer.echo("Parsing GitHub Payload")
        webhookdata = GitHubWebhookData(payload)
    elif payload.get("object_kind"):
        typer.echo("Parsing GitLab Payload")
        webhookdata = GitLabWebhookData(payload)

    if not webhookdata.merged:
        typer.echo("MR / PR is not merged yet! Skipping!")
        typer.echo(webhookdata)
        return DummyPipeline().build()
    else:
        return batch_update_pipeline([RepoRelease(webhookdata.edx_repository, webhookdata.branch_name)])
