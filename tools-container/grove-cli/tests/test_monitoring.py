import re
import uuid

import responses

from grove.monitoring import Monitoring
from grove.newrelic import NEW_RELIC_REGION_US, NewRelicApiUrls

from .utils import GroveTestCase


def _get_new_relic_monitor_response(
    uri="http://newrelic-test.stage.opencraft.hosting/",
):
    monitor_id = str(uuid.uuid4())
    return {
        "apiVersion": "0.2.2",
        "createdAt": "2016-06-06T07:11:51.859+0000",
        "frequency": 5,
        "id": monitor_id,
        "locations": ["AWS_US_EAST_1"],
        "modifiedAt": "2016-06-06T07:11:51.859+0000",
        "name": "test-simple",
        "slaThreshold": 7.0,
        "status": "ENABLED",
        "type": "SIMPLE",
        "uri": uri,
        "userId": 0,
    }


class MonitoringTestCase(GroveTestCase):
    """
    Test Monitoring
    """

    def _mock_create_monitor_response(self):
        """
        Mocks the response for creating a New Relic monitor
        """
        url = "{0}/monitors".format(self.new_relic_urls.synthetics_api_url)
        monitor = _get_new_relic_monitor_response()
        return responses.add(
            responses.POST,
            url,
            json=monitor,
            status=200,
            adding_headers={"Location": url + f'/{monitor["id"]}'},
        )

    def _mock_create_alert_policy(self, name):
        """
        Mocks creating a New Relic alert policy.
        """
        url = "{}.json".format(self.new_relic_urls.alerts_policies_api_url)
        response_json = {"policy": {"id": 1, "name": name}}
        responses.add(responses.GET, url, json={"policies": []})
        responses.add(responses.POST, url, json=response_json, status=201)

    def _mock_existing_alert_policy(self, name):
        """
        Mocks fetching an existing New Relic alert policy.
        """
        url = "{}.json".format(self.new_relic_urls.alerts_policies_api_url)
        response_json = {"policies": [{"id": 1, "name": name}]}
        responses.add(responses.GET, url, json=response_json)

    def _mock_create_notification_channels(self):
        """
        Mocks the creation New Relic notification channels.

        A channel will be created for each email provided and assigned
        the value of policy_ids.
        """
        url = "{}.json".format(self.new_relic_urls.alerts_channels_api_url)
        channel = {
            "id": 1,
            "name": "grove-test@example.com",
            "type": "email",
            "links": {"policy_ids": []},
        }
        responses.add(responses.GET, url, json={"channels": []})
        responses.add(responses.POST, url, json={"channels": [channel]}, status=201)
        policy_channels_url = self.new_relic_urls.alerts_policies_channels_api_url
        responses.add(
            responses.PUT,
            policy_channels_url,
            json={"channel": [channel]},
            status=204,
        )

    def _mock_get_notification_channels(self, emails, policy_ids=None):
        """
        Mocks fetching all New Relic notification channels.

        For each email provided a channel will be returned in the response
        with links to the provided policy_ids.
        """
        url = "{}.json".format(self.new_relic_urls.alerts_channels_api_url)
        response_json = {"channels": []}
        links = {"policy_ids": policy_ids or []}
        for channel_id, email in enumerate(emails):
            response_json["channels"].append(
                {
                    "id": channel_id,
                    "name": f"grove-{email}",
                    "type": "email",
                    "links": links,
                    "configuration": {"recipients": ["test@example.com"]},
                }
            )
        responses.add(responses.GET, url, json=response_json)

    def _mock_get_monitor_response_list(self):
        """
        Mocks fetching all New Relic monitors
        """
        monitors = []
        graphql_response = {"data": {"actor": {"entitySearch": {"results": {"entities": monitors}}}}}
        responses.add(
            responses.POST,
            self.new_relic_urls.nerdgraph_url,
            json=graphql_response,
            status=200,
            adding_headers={"Location": self.new_relic_urls.nerdgraph_url},
        )

    def _mock_get_monitor_response(self):
        """
        Mocks fetching a single New Relic monitor.
        """
        regex = f"{self.new_relic_urls.synthetics_api_url}/monitors/[\\w-]+"
        url = re.compile(regex)
        responses.add(
            responses.GET,
            url,
            json=_get_new_relic_monitor_response(),
            status=200,
            adding_headers={"Location": url},
        )

    def _mock_create_nrql_condition_response(self):
        """
        Adds a mocked response the creation of NRQL conditions for New Relic
        """
        policy_id = 1
        nrql_condition_id = 1
        url = "{}/policies/{}.json".format(self.new_relic_urls.alerts_nrql_conditions_api_url, policy_id)
        responses.add(
            responses.POST,
            url,
            json={"nrql_condition": {"id": nrql_condition_id}},
            status=201,
        )

    def _mock_get_nrql_conditions_response(self):
        """
        Adds a mocked response the creation of NRQL conditions for New Relic
        """
        policy_id = 1
        nrql_condition_id = 1
        url = f"{self.new_relic_urls.alerts_nrql_conditions_api_url}.json?policy_id={policy_id}"

        responses.add(
            responses.GET,
            url,
            json={"nrql_conditions": [{"id": nrql_condition_id}]},
            status=200,
        )

    def _mock_enable_monitoring_requests(self, policy_name):
        self._mock_create_alert_policy(policy_name)
        self._mock_create_monitor_response()
        self._mock_create_notification_channels()

        self._mock_get_monitor_response()
        self._mock_get_monitor_response_list()
        self._mock_create_nrql_condition_response()
        self._mock_get_nrql_conditions_response()

    def setUp(self):
        self.new_relic_urls = NewRelicApiUrls(NEW_RELIC_REGION_US)

    @responses.activate
    def test_enable_monitoring(self):
        """
        Tests the enable_monitoring function on the Monitoring
        instance and verifies that all API calls are hit.
        """
        urls = ["https://example.com"]
        emails = ["test@example.com"]
        instance_name = "test-instance"
        self._mock_enable_monitoring_requests(instance_name)
        monitoring_instance = Monitoring(emails, urls, instance_name, "test-api", NEW_RELIC_REGION_US)
        self._mock_create_alert_policy(instance_name)
        result = monitoring_instance.enable_monitoring()
        self.assertEqual(result.policy_id, 1)
        self.assertEqual(len(result.monitor_ids), 1)

    @responses.activate
    def test_email_address_only_added_once(self):
        """
        Test that when adding an email to monitoring, that it's only done once.

        This method should throw a requests error if that's not the case.
        """
        urls = ["https://example.com"]
        emails = ["test@example.com", "test@example.com"]
        instance_name = "test-instance"
        policy_id = 1
        self._mock_get_notification_channels(emails[:1], [policy_id])
        monitoring_instance = Monitoring(emails, urls, instance_name, "test-api", NEW_RELIC_REGION_US)
        channel_ids = monitoring_instance.update_email_notification_channels(policy_id)
        self.assertEqual(len(channel_ids), 1)

    @responses.activate
    def test_email_address_gets_added_to_policy(self):
        """
        Test that adding an email address to an alert policy works.
        """
        urls = ["https://example.com"]
        emails = ["test@example.com"]
        instance_name = "test-instance"
        policy_id = 1
        self._mock_create_notification_channels()
        self._mock_get_nrql_conditions_response()

        monitoring_instance = Monitoring(emails, urls, instance_name, "test-api", NEW_RELIC_REGION_US)
        channel_ids = monitoring_instance.update_email_notification_channels(policy_id)

        self.assertEqual(len(channel_ids), 1)
