import json
import os
import shutil

import pytest

from .utils import CLEANUP_DIRS, FIXTURES_DIR


@pytest.fixture(scope="session", autouse=True)
def cleanup(request):
    """Cleanup a testing directories once we are finished."""

    def remove_test_dir():
        for dir in CLEANUP_DIRS:
            if os.path.exists(dir):
                shutil.rmtree(dir)

    request.addfinalizer(remove_test_dir)


@pytest.fixture
def gitlab_pipeline_bridges():
    # API response of https://gitlab.com/api/v4/projects/24377526/pipelines/409204040/bridges/
    with open(str(FIXTURES_DIR / "pipeline_bridges_api_response.json")) as fp:
        return json.load(fp)
