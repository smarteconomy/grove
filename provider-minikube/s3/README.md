# S3

This module provisions AWS S3 bucket for the cluster to store the tutor env.

## Required Variables

- `bucket_prefix`: This will be used as the prefix for the bucket name
- `tags`: Tags for attaching to the buckets for identification later

## Outputs

- `s3_bucket` - This contains the output of the `aws_s3_bucket` Terrafrom module.
