variable "minikube_host" {
  type        = string
  description = "URL of your minikube Kubernetes API server."
}
variable "localstack_host" {
  type        = string
  description = "URL of your LocalStack instance."
}

variable "cluster_name" { type = string }

variable "container_registry_server" { default = "registry.gitlab.com" }
variable "dependency_proxy_server" { default = "gitlab.com" }
variable "gitlab_group_deploy_token_username" { type = string }
variable "gitlab_group_deploy_token_password" { type = string }
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "Token retrieved for Gitlab cluster agent"
}
variable "tutor_instances" { type = list(string) }
