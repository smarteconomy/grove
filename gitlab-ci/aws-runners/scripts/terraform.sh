#!/bin/bash

set -euo pipefail

DOCKER_IMAGE="hashicorp/terraform:latest"

function requireVar {
    [[ -z "${2}" ]] && { echo "${1} is required, but not set"; exit 1; } || true;
}

function pullImage {
    image_name="$(echo ${DOCKER_IMAGE} | awk -F ':' '{print $1}')";
    [[ -z "$(docker images | grep ${image_name})" ]] && docker pull "${DOCKER_IMAGE}" || true;
}

function callTerraform {
    requireVar "AWS_ACCESS_KEY_ID" "${AWS_ACCESS_KEY_ID:-}";
    requireVar "AWS_SECRET_ACCESS_KEY" "${AWS_SECRET_ACCESS_KEY:-}";

    docker run -it \
        -v "$(pwd):/workspace" \
        -w /workspace \
        -e AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}" \
        -e AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}" \
        -e TF_VAR_registration_token="${GITLAB_REGISTRATION_TOKEN}" \
        -e TF_VAR_environment="${GROVE_ENVIRONMENT:-production}" \
        "${DOCKER_IMAGE}" ${@:-};
}

pullImage;

terraformCMD="${1}"; shift;
case "${terraformCMD}" in
    init)
        requireVar "GITLAB_PROJECT_ID" "${GITLAB_PROJECT_ID:-}";
        requireVar "GITLAB_USERNAME" "${GITLAB_USERNAME:-}";
        requireVar "GITLAB_ACCESS_TOKEN" "${GITLAB_ACCESS_TOKEN:-}";

        callTerraform "${terraformCMD}" \
            -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/tf-runners-aws" \
            -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/tf-runners-aws/lock" \
            -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/tf-runners-aws/lock" \
            -backend-config="username=${GITLAB_USERNAME}" \
            -backend-config="password=${GITLAB_ACCESS_TOKEN}" "${@:-}";
        ;;
    *)
        requireVar "GITLAB_REGISTRATION_TOKEN" "${GITLAB_REGISTRATION_TOKEN:-}";
        callTerraform "${terraformCMD}" "${@:-}"
        ;;
esac
